<?php

use yii\db\Migration;

/**
 * Handles the creation of table `requisites`.
 */
class m170907_162555_create_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$this->execute("
    	CREATE TABLE `buyer` (
			`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(300) NULL DEFAULT NULL,
			`address` TEXT NULL,
			`inn` VARCHAR(10) NULL DEFAULT NULL,
			`kpp` VARCHAR(10) NULL DEFAULT NULL,
			`check_account` VARCHAR(20) NULL DEFAULT NULL,
			`cor_account` VARCHAR(20) NULL DEFAULT NULL,
			`bik` VARCHAR(9) NULL DEFAULT NULL,
			`bank` VARCHAR(250) NULL DEFAULT NULL,
			PRIMARY KEY (`id`)
		)
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;
		CREATE TABLE `seller` (
			`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(300) NULL DEFAULT NULL,
			`address` TEXT NULL,
			`inn` VARCHAR(10) NULL DEFAULT NULL,
			`kpp` VARCHAR(10) NULL DEFAULT NULL,
			`check_account` VARCHAR(20) NULL DEFAULT NULL,
			`cor_account` VARCHAR(20) NULL DEFAULT NULL,
			`bik` VARCHAR(9) NULL DEFAULT NULL,
			`bank` VARCHAR(250) NULL DEFAULT NULL,
			PRIMARY KEY (`id`)
		)
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;
		CREATE TABLE `bill` (
			`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
			`buyer_id` INT(10) UNSIGNED NULL DEFAULT '0',
			`seller_id` INT(10) UNSIGNED NULL DEFAULT '0',
			`name` VARCHAR(300) NULL DEFAULT '0',
			`price` INT(10) UNSIGNED NULL DEFAULT '0',
			`price_word` VARCHAR(300) NULL DEFAULT '',
			`count` INT(10) UNSIGNED NULL DEFAULT '0',
			`created_at` INT(10) UNSIGNED NULL DEFAULT '0',
			`updated_at` INT(10) UNSIGNED NULL DEFAULT '0',
			PRIMARY KEY (`id`),
			INDEX `buyer_id` (`buyer_id`),
			INDEX `seller_id` (`seller_id`),
			CONSTRAINT `FK_byuer` FOREIGN KEY (`buyer_id`) REFERENCES `buyer` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT `FK_seller` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
		)
		COLLATE='utf8_general_ci'
		ENGINE=InnoDB;
		");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
	    $this->dropTable('bill');
        $this->dropTable('buyer');
	    $this->dropTable('seller');
    }
}
