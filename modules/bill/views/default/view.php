<?php

use app\modules\bill\models\Bill;
use app\modules\bill\models\Buyer;
use app\modules\bill\models\Seller;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $buyer Buyer
 * @var $seller Seller
 * @var $bill Bill
 */
$formatter = Yii::$app->formatter;

?>

<div>Продавец: <?= $buyer->name ?></div>
<div>Адрес: <?= $buyer->address ?></div>
<div>ИНН: <?= $buyer->inn ?></div>
<div>КПП: <?= $buyer->kpp ?></div>
<div>Расчетный счет: <?= $buyer->check_account ?></div>
<div>Кор. счет: <?= $buyer->cor_account ?></div>
<div>БИК: <?= $buyer->bik ?></div>
<div>Банк: <?= $buyer->bank ?></div>
<br><br>
<div>Покупатель: <?= $seller->name ?></div>
<div>Адрес: <?= $seller->address ?></div>
<div>ИНН: <?= $seller->inn ?></div>
<div>КПП: <?= $seller->kpp ?></div>
<div>Расчетный счет: <?= $seller->check_account ?></div>
<div>Кор. счет: <?= $seller->cor_account ?></div>
<div>БИК: <?= $seller->bik ?></div>
<div>Банк: <?= $seller->bank ?></div>

<h1>Счет № <?= $bill->id ?> от <?= $formatter->asDate($bill->created_at) ?></h1>
<table cellpadding="0" cellspacing="0" border="1" width="100%">
    <tr>
        <td>№</td>
        <td>Наименование</td>
        <td>Ед.Изм</td>
        <td>Кол-во</td>
        <td>Цена</td>
        <td>Сумма</td>
    </tr>
    <tr>
        <td>1</td>
        <td><?= $bill->name ?></td>
        <td>шт.</td>
        <td><?= $bill->count ?></td>
        <td><?= $bill->price ?></td>
        <td><?= $bill->price ?></td>
    </tr>
    <tr>
        <td colspan="3">Итого</td>
        <td><?= $bill->count ?></td>
        <td></td>
        <td><?= $bill->price ?></td>
    </tr>
</table>
<div>Сумма прописью: <?= $bill->price_word ?>. Без НДС</div>

<table border="0" width="100%">
    <tr>
        <td>Индивидуальный предпренимательно</td>
        <td valign="bottom">
            <div style="width: 200px; border-bottom: 1px solid #000000"></div>
        </td>
        <td>
            <div style="width: 100px; border-bottom: 1px solid #000000">
                (<span style="display: inline-block; width: 90px;"></span>)
            </div>
        </td>
    </tr>
</table>