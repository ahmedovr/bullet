<?php

use app\modules\bill\models\Bill;
use app\modules\bill\models\Buyer;
use app\modules\bill\models\Seller;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $buyer Buyer
 * @var $seller Seller
 * @var $bill Bill
 */

$form = ActiveForm::begin([
	'id' => 'create-bill-form',
	'options' => ['class' => 'form-horizontal'],
])
?>

<div class="bill-default-index">
    <h1>Создание счета</h1>

    <h2>Продавец:</h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'name'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'address'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'inn'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'kpp'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'check_account'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
			    <?= $form->field($buyer, 'cor_account'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'bik'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
	            <?= $form->field($buyer, 'bank'); ?>
            </div>
        </div>
    </div>

    <h2>Покупатель:</h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'name'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'address'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'inn'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'kpp'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'check_account'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
			    <?= $form->field($seller, 'cor_account'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'bik'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($seller, 'bank'); ?>
            </div>
        </div>
    </div>

    <h2>Счет:</h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($bill, 'name'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
				<?= $form->field($bill, 'price'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
			    <?= $form->field($bill, 'price_word'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
			    <?= $form->field($bill, 'count'); ?>
            </div>
        </div>
    </div>

	<?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end() ?>