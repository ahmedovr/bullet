<?php

namespace app\modules\bill\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "bill".
 *
 * @property string $id
 * @property string $buyer_id
 * @property string $seller_id
 * @property string $name
 * @property string $price
 * @property string $price_word
 * @property string $count
 * @property string $created_at
 */
class Bill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill';
    }

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['buyer_id', 'seller_id', 'price', 'price_word', 'count'], 'required'],
	        [['buyer_id', 'seller_id', 'price', 'count', 'created_at'], 'integer'],
	        [['name', 'price_word'], 'string', 'max' => 300],
	        [['buyer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Buyer::className(), 'targetAttribute' => ['buyer_id' => 'id']],
	        [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seller::className(), 'targetAttribute' => ['seller_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'buyer_id' => 'Buyer ID',
            'seller_id' => 'Seller ID',
            'name' => 'Наименование',
            'price' => 'Цена',
	        'price_word' => 'Стоимость прописью',
	        'count' => 'Количество',
            'created_at' => 'Created At',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBuyer()
	{
		return $this->hasOne(Buyer::className(), ['id' => 'buyer_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSeller()
	{
		return $this->hasOne(Seller::className(), ['id' => 'seller_id']);
	}
}
