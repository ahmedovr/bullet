<?php

namespace app\modules\bill\models;

use Yii;

/**
 * This is the model class for table "buyer".
 *
 * @property string $id
 * @property string $name
 * @property string $address
 * @property string $inn
 * @property string $kpp
 * @property string $check_account
 * @property string $cor_account
 * @property string $bik
 * @property string $bank
 */
class Buyer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buyer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
	        [['address', 'name', 'inn', 'kpp', 'check_account', 'cor_account', 'bik', 'bank'], 'required'],
            [['address'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['inn', 'kpp'], 'string', 'max' => 10],
            [['check_account', 'cor_account'], 'string', 'max' => 20],
            [['bik'], 'string', 'max' => 9],
            [['bank'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'address' => 'Адрес',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'check_account' => 'Р/С',
            'cor_account' => 'К/С',
            'bik' => 'БИК',
            'bank' => 'Банк',
        ];
    }
}
