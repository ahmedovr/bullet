<?php

namespace app\modules\bill\controllers;

use app\modules\bill\models\Bill;
use app\modules\bill\models\BillSearch;
use app\modules\bill\models\Buyer;
use app\modules\bill\models\Seller;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `bill` module
 */
class DefaultController extends Controller
{
    /**
     * Главная страница с кнопками для создания и списком счетов
     *
     * @return string
     */
    public function actionIndex()
    {
	    $searchModel = new BillSearch();
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	    return $this->render('list', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
	    ]);
    }

	/**
	 * Станица с созданием нового счета
	 *
	 * @return string
	 */
	public function actionCreate()
	{
		$buyer = new Buyer();
		$seller = new Seller();
		$bill = new Bill();

		if ($buyer->load(Yii::$app->request->post()) && $seller->load(Yii::$app->request->post()) && $bill->load(Yii::$app->request->post())) {
			$transaction = Yii::$app->db->beginTransaction();

			if ($buyer->save() && $seller->save()) {
				$bill->buyer_id = $buyer->id;
				$bill->seller_id = $seller->id;
				$bill->save();
				$transaction->commit();
				$this->redirect(['/bill/default/view', 'id' => $bill->id]);
			} else {
				$transaction->rollback();
			}
		}

		return $this->render('create', [
			'buyer' => $buyer,
			'seller' => $seller,
			'bill' => $bill
		]);
	}

	/**
	 * Страница просмотра созданного счета
	 *
	 * @param int $id
	 *
	 * @return string
	 */
	public function actionView($id)
	{
		$bill = Bill::findOne($id);

		return $this->render('view', [
			'buyer' => $bill->buyer,
			'seller' => $bill->seller,
			'bill' => $bill
		]);
	}
}
